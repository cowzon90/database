﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDBSample.MDB
{
    public partial class MDB
    {

        public void CreateDataTable(DataTable dataTable, string tableName)
        {

        }

        public void CreateDataTable(List<string> fields, List<string> types)
        {

        }

        /// <summary>
        /// Table 생성 Query를 생성.
        /// </summary>
        /// <param name="tableName">테이블 명</param>
        /// <param name="fields">필드 리스트</param>
        /// <returns></returns>
        private string CreateTableQuery(string tableName, List<string> fields, List<string> types)
        {
            if (fields.Count != types.Count)
            {
                throw new Exception("필드 명 리스트와 필드 타입 리스트의 수가 다릅니다.");
            }

            try
            {
                string createSql = "CREATE TABLE [" + tableName + "]";
                createSql += " (";

                for (int i = 0; i < fields.Count; i++)
                {
                    createSql += fields[i];
                    createSql += " " + types[i];
                    if (i != fields.Count - 1)
                    {
                        createSql += ", ";
                    }
                }

                createSql += ")";

                return createSql;
            }
            catch
            {
                throw;
            }
        }

    }
}
