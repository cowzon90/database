﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDBSample.MDB
{
    public partial class MDB
    {
        /// <summary>
        /// Insert query Text
        /// </summary>
        private string insertText { get; set; }

        public bool InsertDataTable(DataTable dataTable, string tableName )
        {
            try
            {
                List<string> fields = this.MakeFieldNames(dataTable);    // Field List
                List<string> types = this.MakeFieldTypes(dataTable);      // Types List
                string query = this.CreateTableQuery(tableName, fields, types);     // create query.

                // Create Data Table
                this.connection.Open();
                OleDbCommand command = new OleDbCommand(query, this.connection);
                command.ExecuteNonQuery();

                // Copy All Data To Destination

                foreach (DataRow row in dataTable.Rows)
                {
                    this.RunInsertCommand(row);
                }

                return true;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                this.connection.Close();
            }
        }

        /// <summary>
        /// Data Table 이름과 필드를 이용하여 Insert query 생성.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="fields"></param>
        private void SetInsertText(string tableName, List<string> fields)
        {
            try
            {
                // make command text
                string commandText = @"INSERT INTO " + "[" + tableName + "]" + "(";
                for (int i = 0; i < fields.Count; i++)
                {
                    commandText += fields[i];
                    if (i != fields.Count - 1)
                    {
                        commandText += ", ";
                    }
                }
                commandText += ")";

                // Values
                commandText += " VALUES(";
                for (int i = 0; i < fields.Count; i++)
                {
                    commandText += "@param" + i.ToString();
                    if (i != fields.Count - 1)
                    {
                        commandText += ", ";
                    }
                }
                commandText += ")";

                this.insertText = commandText;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///  DataTable의 row를 대상으로 Insert 명령 수행
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private void RunInsertCommand(DataRow row)
        {
            try
            {
                OleDbCommand command = new OleDbCommand();
                command.Connection = this.connection;
                
                // Add params
                command.CommandText = this.insertText;
                command.Parameters.Clear();
                for (int i = 0; i < row.ItemArray.Length; i++)
                {
                    command.Parameters.AddWithValue("@param" + i.ToString(), row[i]);
                }

                command.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Column에 따른 필드 타입을 리스트로 반환
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        private List<string> MakeFieldTypes(DataTable dataTable)
        {
            try
            {
                List<string> types = new List<string>();
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    string type = this.GetType(dataTable.Columns[i].DataType);
                    types.Add(type);
                }

                return types;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// TODO : More Details.
        /// Data Type을 String으로 변경
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private string GetType(Type type)
        {
            switch (type.Name)
            {
                #region INTEGER
                case "Int64":
                    return "INTEGER";
                case "Int32":
                    return "INTEGER";
                case "Int16":
                    return "INTEGER";
                #endregion

                #region STRING
                case "String":
                    return "CHAR(255)";

                #endregion
                case "Single":
                    return "DOUBLE";
                case "Double":
                    return "DOUBLE";
                default:
                    throw new Exception("데이터 타입 함수를 수정하세요.");
                    return "CHAR(255)";
            }
        }

        /// <summary>
        /// Column에 따른 필드 명을 리스트로 반환
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        private List<string> MakeFieldNames(DataTable dataTable)
        {
            try
            {
                List<string> fields = new List<string>();
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    string fieldName = dataTable.Columns[i].ColumnName;
                    fields.Add(fieldName);
                }

                return fields;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
