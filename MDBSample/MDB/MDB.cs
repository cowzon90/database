﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDBSample.MDB
{
    /// <summary>
    /// MDB Class
    /// </summary>
    public partial class MDB
    {
        /// <summary>
        /// Connection String
        /// </summary>
        private string connectionString { get; set; }

        /// <summary>
        /// Data Base connection
        /// </summary>
        private OleDbConnection connection { get; set; }

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="path">DB 경로</param>
        /// <param name="password">DB 패스워드</param>
        public MDB(string path, string password)
        {
            this.connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Jet OLEDB:Database Password=" + password;
            this.connection = new OleDbConnection(this.connectionString);
        }

        /// <summary>
        /// Default Contructor
        /// </summary>
        public MDB()
        {
            
        }

        /// <summary>
        ///  Connection Close
        /// </summary>
        public void Close()
        {
            try
            {
                this.connection.Close();
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// 특정 스트링이 Braket('[', ']')로 감싸여 있지 않으면 감싸 string 리턴.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private string CheckString(string str)
        {
            try
            {
                char[] chars = str.ToCharArray();
                bool isFirstBraket = true;

                if(chars[0] != '[')
                {
                    isFirstBraket = false;
                }

                bool isLastBraket = true;
                if(chars[chars.Length -1] != ']')
                {
                    isLastBraket = false;
                }

                if (!isFirstBraket)
                {
                    str = "[" + str;
                }

                if (!isLastBraket)
                {
                    str += "]";
                }

                return str;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// table name과 일치하는 Data Table 반환.
        /// </summary>
        /// <param name="tableName">테이블 명</param>
        /// <returns></returns>
        public DataTable GetDataTable(string tableName)
        {
            try
            {
                DataTable dt = new DataTable();

                tableName = "[" + tableName + "]";
                string query = string.Format("SELECT * FROM {0}", tableName);

                OleDbDataAdapter adapter = new OleDbDataAdapter(query, this.connection);
                adapter.Fill(dt);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Insert Query 생성.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private string MakeInsertQuery(string tableName, List<string> fields, DataRow row)
        {
            try
            {
                // INSERT INTO TABLE_NAME
                string queryInsert = "INSERT INTO " + tableName;

                // Set Field Names
                queryInsert += " (";
                for(int i = 0; i < fields.Count; i++)
                {
                    queryInsert += "[" + fields[i] + "]";
                    if(i != fields.Count - 1)
                    {
                        queryInsert += ", ";
                    }
                }
                queryInsert += ")";

                // Set Values
                queryInsert += " VALUES(";
                for (int i = 0; i < row.ItemArray.Length; i++)
                {
                    queryInsert += "[" + row[i].ToString() + "]";

                    // TODO 
                    if (i != row.ItemArray.Length - 1)
                    {
                        queryInsert += ", ";
                    }
                }
                queryInsert += ")";

                return queryInsert;
            }
            catch (Exception)
            {

                throw;
            }
        }

        
        /// <summary>
        /// Create DataTable to path with table name.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="password"></param>
        /// <param name="tableName"></param>
        public void CreateTable(string path, string password, string tableName)
        {
            try
            {
                String strDBCon = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Jet OLEDB:Database Password={1}", path, password);
                OleDbConnection conn = new OleDbConnection(strDBCon);
                conn.Open();

                //테이블 생성
                string sql = string.Format("CREATE TABLE {0} (INFLOW INT)", tableName);
                OleDbCommand cmd = new OleDbCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
