﻿using ADOX;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDBSample
{
    public class MDBDao
    {
        public DataTable GetMDBData(string path, string tableName)
        {
            try
            {
                DataTable dt = new DataTable();

                string connStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Jet OLEDB:Database Password=krc_fcsr";
                OleDbConnection conn = new System.Data.OleDb.OleDbConnection(connStr);
                string sql = string.Format("SELECT * FROM {0}", tableName);

                OleDbDataAdapter adapter = new OleDbDataAdapter(sql, conn);
                adapter.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataTable GetMDBData(string path, string tableName, string password)
        {
            try
            {
                DataTable dt = new DataTable();

                string connStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Jet OLEDB:Database Password=" + password;
                OleDbConnection conn = new System.Data.OleDb.OleDbConnection(connStr);
                string sql = string.Format("SELECT * FROM {0}", tableName);

                OleDbDataAdapter adapter = new OleDbDataAdapter(sql, conn);
                adapter.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void CreateMDBFile(string path, string password)
        {
            try
            {
                String strDBCon = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Jet OLEDB:Database Password={1}", path, password);

                CatalogClass adoxCC = new CatalogClass();
                adoxCC.Create(strDBCon);
                adoxCC.ActiveConnection = null;
                adoxCC = null;

            }
            catch (Exception ex)
            {

                throw ex;
            }
 
        }

        public void CreateTable(string path, string password, string tableName)
        {
            try
            {
                String strDBCon = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Jet OLEDB:Database Password={1}", path, password);
                OleDbConnection conn = new OleDbConnection(strDBCon);
                conn.Open();

                //테이블 생성
                string sql = string.Format("CREATE TABLE {0} (INFLOW INT)", tableName);
                OleDbCommand cmd = new OleDbCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void InsertData(string path, string password, string tableName, string value)
        {
            try
            {
                String strDBCon = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Jet OLEDB:Database Password={1}", path, password);
                OleDbConnection conn = new OleDbConnection(strDBCon);
                conn.Open();

                string sql = string.Format("INSERT INTO {0} VALUES({1})", tableName, value);
                OleDbCommand cmd = new OleDbCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void InsertData(string path, string password, string tableName, DateTime datetime, string code, double value)
        {
            try
            {
                String strDBCon = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Jet OLEDB:Database Password={1}", path, password);
                OleDbConnection conn = new OleDbConnection(strDBCon);
                conn.Open();

                string sql;

                switch (tableName)
                {
                    case "Rain_HrData":

                        sql = string.Format("INSERT INTO [{0}] ([Dates], [Code], [Rainfall]) VALUES ('{1}', '{2}', '{3}')", tableName, datetime, code, value);

                        break;
                    default:

                        sql = null;
                        break;
                }

                OleDbCommand cmd = new OleDbCommand(sql, conn);
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                if(!System.IO.File.Exists(
                    System.IO.Path.Combine(
                        System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt")))
                {
                    System.IO.File.Create(System.IO.Path.Combine(
                        System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt"));
                }

                System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.Combine(System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt"), append: true);
                sw.WriteLine(tableName + " " + datetime.ToString() + " " + code + " " + value.ToString());
                sw.Close();
                throw ex;
            }
        }
    }
}
