﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MDBSample
{
    class Program
    {
        static void Main(string[] args)
        {
            MDB.MDB hdsCommon = new MDB.MDB(@"D:\Development\HDS\HDS\MagpieSoft.HDS.View\DB\HDSCommon.mdb", "krc_homwrs");
            MDB.MDB rms = new MDB.MDB(@"D:\Development\HDS\HDS\MagpieSoft.HDS.View\DB\RMS.mdb", "krc_rms");

            // Read rms data
            DataTable rmsDt = rms.GetDataTable("YearMax");

            hdsCommon.InsertDataTable(rmsDt, "YearMax");

        }

        private void MDBCopySample()
        {
            // 1. MDB 파일 읽어오기
            MDBDao dao = new MDBDao();
            string mdbPath = "";
            string tableName = "";

            DataTable dt = dao.GetMDBData(mdbPath, tableName);


            // 2. 새로운 MDB 파일 생성하기
            string newMdbPath = "";
            string password = "";
            string newTableName = "";
            dao.CreateMDBFile(newMdbPath, password);


            // 3. 새로운 MDB 파일의 테이블 생성하기
            dao.CreateTable(newMdbPath, password, newTableName);


            // 4. 새로운 MDB 파일의 데이터 삽입하기
            foreach (DataRow dr in dt.Rows)
            {
                dao.InsertData(newMdbPath, password, newTableName, dr[0].ToString());
            }
        }

        private void MDBCopy()
        {
            // 원본 DB Read
            MDBDao origin = new MDBDao();
            DataTable originDt = origin.GetMDBData("D:\\Development\\HDS\\FCSR.mdb", "Rain_HrData", "krc_fcsr");

            // 관측소 코드
            List<string> Codes = new List<string>();
            DataTable CodesDT = origin.GetMDBData("D:\\Development\\HDS\\FCSR.mdb", "Info_Station", "krc_fcsr");
            foreach (DataRow row in CodesDT.Rows)
            {
                Codes.Add(row[2].ToString());
            }

            // DB Write
            string path = "D:\\Development\\HDS\\Rainfall.MDB";
            string password = "111";
            string tableName = "Rain_HrData";

            try
            {
                String strDBCon = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Jet OLEDB:Database Password={1}", path, password);
                OleDbConnection conn = new OleDbConnection(strDBCon);
                conn.Open();

                // 관측소 코드 별 루프
                foreach (string code in Codes)
                {
                    DataRow[] rows = originDt.Select(string.Format("Code='{0}'", code));

                    foreach (DataRow row in rows)
                    {
                        string date = Convert.ToDateTime(row[2]).ToString("yyyy'-'MM'-'dd");

                        for (int i = 0; i < 24; i++)
                        {
                            string datetime = date + string.Format(" {0}:00:00", i.ToString("D2"));

                            DateTime datetime2 = Convert.ToDateTime(datetime);

                            double value;
                            if (row[3 + i] != DBNull.Value)
                            {
                                value = Convert.ToDouble(row[3 + i]);
                            }
                            else
                            {
                                value = 0.0;
                            }

                            string sql = string.Format("INSERT INTO [{0}] ([Dates], [Code], [Rainfall]) VALUES ('{1}', '{2}', '{3}')", tableName, datetime, code, value);

                            OleDbCommand cmd = new OleDbCommand(sql, conn);
                            cmd.ExecuteNonQuery();
                        }
                    }
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                //if (!System.IO.File.Exists(
                //    System.IO.Path.Combine(
                //        System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt")))
                //{
                //    System.IO.File.Create(System.IO.Path.Combine(
                //        System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt"));
                //}

                //System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.Combine(System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt"), append: true);
                //sw.WriteLine(tableName + " " + datetime.ToString() + " " + code + " " + value.ToString());
                //sw.Close();
                throw ex;
            }
        }

        private void MDB100Test()
        {
            // 원본 DB Read
            MDBDao origin = new MDBDao();
            DataTable originDt = origin.GetMDBData("D:\\Development\\HDS\\FCSR.mdb", "Rain_HrData", "krc_fcsr");

            // COPY
            string copy_path = "D:\\Development\\HDS\\Rainfall_copy.mdb";

            // Test
            string test_path = "D:\\Development\\HDS\\Rainfall_test.mdb";

            string password = "111";
            string tableName = "Rain_HrData";

            try
            {
                // Test
                String test_StrDBCon = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Jet OLEDB:Database Password={1}", test_path, password);
                OleDbConnection test_conn = new OleDbConnection(test_StrDBCon);
                test_conn.Open();

                for (int i = 0; i < 100; i++)
                {
                    // Origin DataRow
                    DataRow row = originDt.Rows[i];

                    string date = Convert.ToDateTime(row[2]).ToString("yyyy'-'MM'-'dd");
                    for (int j = 0; j < 24; j++)
                    {
                        string datetime = date + string.Format(" {0}:00:00", j.ToString("D2"));

                        DateTime datetime2 = Convert.ToDateTime(datetime);

                        double value;
                        if (row[3 + j] != DBNull.Value)
                        {
                            value = Convert.ToDouble(row[3 + j]);
                        }
                        else
                        {
                            value = 0.0;
                        }

                        // Test
                        string sql = string.Format("INSERT INTO [{0}] ([Dates], [Code], [Rainfall]) VALUES ('{1}', '{2}', '{3}')", tableName, datetime2, row[1].ToString(), value);

                        OleDbCommand cmd = new OleDbCommand(sql, test_conn);
                        cmd.ExecuteNonQuery();

                    }
                }

                test_conn.Close();
            }
            catch (Exception ex)
            {
                //if (!System.IO.File.Exists(
                //    System.IO.Path.Combine(
                //        System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt")))
                //{
                //    System.IO.File.Create(System.IO.Path.Combine(
                //        System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt"));
                //}

                //System.IO.StreamWriter sw = new System.IO.StreamWriter(System.IO.Path.Combine(System.Reflection.Assembly.GetExecutingAssembly().Location, "Error.txt"), append: true);
                //sw.WriteLine(tableName + " " + datetime.ToString() + " " + code + " " + value.ToString());
                //sw.Close();
                throw ex;
            }

        }
    }
}
